# -*- coding: utf-8 -*-
import org.bukkit as bukkit
from org.bukkit.inventory import ItemStack
from org.bukkit.potion import PotionEffect, PotionEffectType
from org.bukkit.enchantments import Enchantment
from org.bukkit import Material, ChatColor, Sound
from org.bukkit.util import Vector
from org.bukkit.entity import Player, EntityType
from org.bukkit import ChatColor as CC
from books import Book
from strings import strings
from net.citizensnpcs.api.trait.trait import Equipment
#from net.citizensnpcs.api import CitizensPlugin
import random, time, thread

server = bukkit.Bukkit.getServer()
cit = server.getPluginManager().getPlugin('Citizens')

def doLater(action, sleep, *args, **kwargs):
    def wrap():
        t = time.time()
        while time.time()-t < sleep:
            time.sleep(0.5)
        return action(*args, **kwargs)
    thread.start_new_thread(wrap, ())

class BaseDwarf(object):
    def __init__(self, **kwargs):
        self.name = None  #Dwarf type
        self.hashedItems = None  #set of hashed items
        self.cooldown = 0  #Cooldown holder
        self.ctime = 30  #Cooldown time
        self.match_book = False
        self.book = Book()
        self.implementsLevel = False
        self.implementsHit = False
        self.__dict__.update(kwargs)
        self.req_items = [(337, 1), (370, 1), (Material.SUGAR, 1)]

    def die(self, plyr): pass
    def start(self, inv, srv): pass
    def spell(self, ply, right=False, book=None): pass
    def getHit(self, plyr, atk, patk, pdef): pass
    def clicky(self, obj, data, **kwargs):
        p = obj.getPlayer()
        if obj.getItem().getTypeId() == 337:  #Health
            p.addPotionEffect(PotionEffect(PotionEffectType.HEAL, 1, 1))
        elif obj.getItem().getTypeId() == 370:
            p.addPotionEffect(PotionEffect(PotionEffectType.REGENERATION, 900, 1))
        elif obj.getItem().getTypeId() == 353:
            p.addPotionEffect(PotionEffect(PotionEffectType.SPEED, 600, 2))
        else: return
        p.getWorld().playSound(p.getLocation(), Sound.BURP, 10, 1)
        p.getInventory().removeItem(obj.getItem())

class Builder(BaseDwarf):
    def __init__(self):
        BaseDwarf.__init__(self, name="builder", ctime=30)
        self.book = Book(title="Builder", author="Janus", pages=strings['builder'])
        self.color = 000000255
        self.nameplate = CC.DARK_BLUE

    def start(self, inv, server):
        if not self.hashedItems:
            i = []
            for r in [(Material.COOKED_BEEF, 5), (Material.TORCH, 32), (Material.GOLD_AXE, 1), (Material.WOOD_PICKAXE, 1), (Material.SMOOTH_BRICK, 64), (Material.SMOOTH_BRICK, 64), (10, 1)]+self.req_items:
                i.append(ItemStack(*r))
            for f in [Material.IRON_PICKAXE, Material.IRON_SPADE, Material.IRON_AXE]:
                p = ItemStack(f)
                p.addEnchantment(Enchantment.DURABILITY, 3)
                i.append(p)
            i.append(self.book.dump())
            self.hashedItems = i
        inv.addItem(*self.hashedItems)

    def spell(self, player, right=False, book=None):
        inv = player.getInventory()
        if time.time()-self.cooldown >= self.ctime:
            self.cooldown = time.time()
            for i in range(1, random.randint(2, 4)):
                inv.addItem(ItemStack(Material.SMOOTH_BRICK, random.choice([16, 32, 64])))
                inv.addItem(ItemStack(Material.SMOOTH_BRICK, random.choice([16, 32, 64])))

            inv.addItem(ItemStack(Material.SMOOTH_STAIRS, random.randint(5, 10)))
            inv.addItem(ItemStack(Material.LEVER, random.randint(1, 3)))
            inv.addItem(ItemStack(Material.REDSTONE_LAMP_OFF, random.randint(1, 3)))

            player.sendMessage("%sYou used the Builders spell!" % ChatColor.YELLOW)
            player.updateInventory()
            return True
        else:
            x = self.ctime-int(round(time.time()-self.cooldown))
            player.sendMessage("%sSpell is still on cooldown (%ss)!" % (ChatColor.YELLOW, str(x)))
            return False

    def clicky(self, obj, data, **kwargs):
        BaseDwarf.clicky(self, obj, data)
        if obj.getItem().getTypeId() == 286:  #Gold axe
            check = data['blockhash']['tables']
            b = obj.getClickedBlock()
            if b.getLocation().distance(data['blockhash']['egg']) < 5:
                return obj.getPlayer().sendMessage('%sYou cant place blocks that close too the core!' % ChatColor.YELLOW)
            blcks = []
            for x in [-1, 0, 1]:
                for y in [0, 1, 2]:
                    for z in [-1, 0, 1]:
                        loc = bukkit.Location(b.getWorld(), b.getX()+x, b.getY()+y, b.getZ()+z)
                        if loc in check: return
                        blcks.append(loc)

            for bl in blcks:
                bl.getBlock().setType(Material.SMOOTH_BRICK)
            dura = obj.getItem().getDurability()+1
            if dura > 33:
                return obj.getPlayer().getInventory().removeItem(obj.getItem())
        elif obj.getItem().getTypeId() == 270: #Wood pick
            check = data['blockhash']['tables']
            b = obj.getClickedBlock()
            if b.getLocation().distance(data['blockhash']['egg']) < 5:
                return obj.getPlayer().sendMessage('%sYou cant place blocks that close too the core!' % ChatColor.YELLOW)
            blcks = []
            for y in [0, 1, 2, 3, 4]:
                loc = bukkit.Location(b.getWorld(), b.getX(), b.getY()+y, b.getZ())
                if loc in check: return
                blcks.append(loc)
            for bl in blcks:
                bl.getBlock().setTypeId(112) #Nether Brick
            dura = obj.getItem().getDurability()+1
            if dura > 60:
                return obj.getPlayer().getInventory().removeItem(obj.getItem())
        else: return
        obj.getItem().setDurability(dura)

class Miner(BaseDwarf):
    def __init__(self):
        BaseDwarf.__init__(self, name="miner", ctime=15)
        self.book = Book(title="Miner", author="Hathor", pages=strings['miner'])
        self.color = 000000255
        self.nameplate = CC.DARK_RED

    def start(self, inv, server):
        if not self.hashedItems:
            i = []
            for r in [(Material.COOKED_BEEF, 5), (Material.TORCH, 64), (Material.CHEST, 1), (Material.BUCKET, 1)]+self.req_items:
                i.append(ItemStack(*r))
            p = ItemStack(278, 1)
            #p.addEnchantment(Enchantment.LOOT_BONUS_BLOCKS, 3)
            p.addEnchantment(Enchantment.DIG_SPEED, 5)
            i.append(p)
            i.append(self.book.dump())
            self.hashedItems = i
        inv.addItem(*self.hashedItems)

    def spell(self, player, right=False, book=None):
        if right: return False
        inv = player.getInventory()
        if inv.contains(Material.COBBLESTONE, 64):
            if time.time()-self.cooldown >= self.ctime:  #15 second cooldown
                self.cooldown = time.time()
                x = [inv.getItem(i) for i in inv.all(Material.COBBLESTONE) if inv.getItem(i).getAmount() >= 64]
                for lawl in x:
                    inv.removeItem(x)
                    inv.addItem(ItemStack(Material.REDSTONE, random.randint(20, 40)))
                player.sendMessage("%sYou used the Miners spell!" % ChatColor.YELLOW)
                player.updateInventory()
                return False
            else:
                x = self.ctime-(int(round(time.time()-self.cooldown)))
                player.sendMessage("%sSpell is still on cooldown (%ss)!" % (ChatColor.YELLOW, str(x)))
                return False
        else:
            player.sendMessage("%sYou need at least 64 cobble to perform the Miners spell!" % ChatColor.YELLOW)
            return False

class Baker(BaseDwarf):
    def __init__(self):
        BaseDwarf.__init__(self, name="baker", ctime=1)
        self.book = Book(title="Baker", author="Hestia", pages=strings['baker'])
        self.color = 000000255
        self.nameplate = CC.GOLD

    def start(self, inv, server):
        if not self.hashedItems:
            i = []
            for r in [(336, 1), (Material.COOKED_BEEF, 5), (Material.TORCH, 16), (Material.SANDSTONE, 64), (Material.SANDSTONE, 64), (Material.CHEST, 2)]+self.req_items:
                i.append(ItemStack(*r))
            for r in [(Material.STONE_PICKAXE, 1), (Material.STONE_SPADE, 1), (Material.STONE_AXE, 1)]:
                p = ItemStack(*r)
                p.addEnchantment(Enchantment.DURABILITY, 3)
                i.append(p)
            i.append(self.book.dump())
            self.hashedItems = i
        inv.addItem(*self.hashedItems)

    def spell(self, player, right=False, book=None):
        inv = player.getInventory()
        if inv.contains(Material.REDSTONE, 15):
            x = [inv.getItem(i) for i in inv.all(Material.REDSTONE) if inv.getItem(i).getAmount() >= 15]
            for lawl in x:
                i = 0
                while lawl.getAmount() >= 15:
                    if lawl.getAmount() <= 15: inv.removeItem(lawl)
                    else:
                        lawl.setAmount(int(lawl.getAmount())-15)
                        inv.addItem(ItemStack(Material.WHEAT, random.randint(10, 30)))
                        inv.addItem(ItemStack(Material.FLINT, 1))
                        inv.addItem(ItemStack(Material.COAL, random.randint(2, 8)))
                    if right: break
                    i += 1
                    if i > 15: break
                if right: break
            player.sendMessage("%sYou used the Bakers spell!" % ChatColor.YELLOW)
            player.updateInventory()
            return True
        else:
            player.sendMessage("%sYou need at least 15 redstone (dust) to perform the Bakers spell" % ChatColor.YELLOW)
            return False

    def clicky(self, obj, data, **kwargs):
        BaseDwarf.clicky(self, obj, data)
        p = obj.getPlayer()
        if obj.getItem().getTypeId() == 336:  #Brick
            if time.time()-self.cooldown >= self.ctime:
                self.cooldown = time.time()
                loc = p.getLocation()
                li = [1, -1]
                for i in range(1, 10):
                    l = loc.clone()
                    l.setX((loc.getX()*random.choice(li)))
                    l.setZ((loc.getZ()*random.choice(li)))
                    b = p.getWorld().spawnFallingBlock(l, Material.CAKE_BLOCK, 0)
                    b.setDropItem(False)
                    b.setVelocity(b.getLocation().toVector().subtract(p.getLocation().toVector()).normalize().multiply(1).setY(1))
            else:
                x = self.ctime-(int(round(time.time()-self.cooldown)))
                p.sendMessage("%sSpell is still on cooldown (%ss)!" % (ChatColor.YELLOW, str(x)))
                return False

class BlackSmith(BaseDwarf):
    def __init__(self):
        BaseDwarf.__init__(self, name="blacksmith")
        self.book = Book(title="Blacksmith", author="Hephaestus", pages=strings['blacksmith'])
        self.color = 000000255
        self.nameplate = CC.BLACK

    def start(self, inv, server):
        if not self.hashedItems:
            i = []
            for item in [(Material.COOKED_BEEF, 5), (Material.TORCH, 16), (Material.BRICK, 64), (Material.CHEST, 2)]+self.req_items:
                i.append(ItemStack(*item))
            for r in [(Material.STONE_PICKAXE, 1), (Material.STONE_SPADE, 1), (Material.STONE_AXE, 1)]:
                p = ItemStack(*r)
                p.addEnchantment(Enchantment.DURABILITY, 3)
                i.append(p)
            i.append(self.book.dump())
            self.hashedItems = i
        inv.addItem(*self.hashedItems)

    def spell(self, player, right=False, book=None):
        inv = player.getInventory()
        if inv.contains(Material.REDSTONE, 15):
            x = [inv.getItem(i) for i in inv.all(Material.REDSTONE) if inv.getItem(i).getAmount() >= 15]
            for lawl in x:
                i = 0
                while lawl.getAmount() >= 15:
                    if lawl.getAmount() <= 15: inv.removeItem(lawl)
                    else:
                        x = int(lawl.getAmount())-15
                        lawl.setAmount(x)
                    inv.addItem(ItemStack(Material.COAL, random.randint(2, 5)))
                    inv.addItem(ItemStack(Material.RED_MUSHROOM, random.randint(5, 10)))
                    inv.addItem(ItemStack(Material.ARROW, random.randint(10, 20)))
                    inv.addItem(ItemStack(Material.IRON_FENCE, random.randint(5, 10)))
                    if right: break
                    i += 1
                    if i > 15: break
                if right: break      
            player.sendMessage("%sYou used the Blacksmiths spell!" % ChatColor.YELLOW)
            player.updateInventory()
            return True
        else:
            player.sendMessage("%sYou need at least 15 redstone (dust) to perform the Blacksmiths spell!" % ChatColor.YELLOW)
            return False

class Alchemist(BaseDwarf):
    def __init__(self):
        BaseDwarf.__init__(self, name="alchemist")
        self.book = Book(title="Alchemist", author="Hermes", pages=strings['alchemist'])
        self.color = 000000255
        self.nameplate = CC.DARK_PURPLE

    def start(self, inv, server):
        if not self.hashedItems:
            i = []
            for r in [(Material.COOKED_BEEF, 5), (Material.TORCH, 16), (Material.NETHER_BRICK, 64), (Material.NETHER_BRICK, 64), (Material.CHEST, 2), (Material.BLAZE_ROD, 10)]+self.req_items:
                i.append(ItemStack(*r))
            for r in [(Material.STONE_PICKAXE, 1), (Material.STONE_SPADE, 1), (Material.STONE_AXE, 1)]:
                p = ItemStack(*r)
                p.addEnchantment(Enchantment.DURABILITY, 3)
                i.append(p)
            i.append(self.book.dump())
            self.hashedItems = i
        inv.addItem(*self.hashedItems)

    def spell(self, player, right=False, book=None):
        inv = player.getInventory()
        if inv.contains(Material.REDSTONE, 20):
            x = [inv.getItem(i) for i in inv.all(Material.REDSTONE) if inv.getItem(i).getAmount() >= 20]
            for lawl in x:
                i = 0
                while lawl.getAmount >= 20:
                    if lawl.getAmount() <= 20: inv.removeItem(lawl)
                    else:
                        x = int(lawl.getAmount())-20
                        lawl.setAmount(x)  
                    for i in [Material.NETHER_WARTS, Material.SUGAR, Material.SPECKLED_MELON, Material.GHAST_TEAR, Material.MAGMA_CREAM]:
                        inv.addItem(ItemStack(i, random.randint(3, 7)))
                    if right: break
                    i += 1
                    if i > 15: break
                if right: break
            player.sendMessage("%sYou used the Alchemists spell!" % ChatColor.YELLOW)
            player.updateInventory()
            return True
        else:
            player.sendMessage("%sYou need at least 20 redstone (dust) to perform the Alchemists spell!" % ChatColor.YELLOW)
            return False

class Mage(BaseDwarf):
    def __init__(self):
        BaseDwarf.__init__(self, name="mage", ctime=30)
        self.book = Book(title="The Mage", author="Magikarp", pages=strings['mage'])
        #self.implementsLevel = True
        self.implementsHit = True
        self.color = 000000255
        self.nameplate = CC.GREEN

        self.cloaked = False
        self.npc = None
        self.holoc = 0
        self.holotime = 90
        self.clockstart = 0

    def die(self, plyr):
        self.npcDestroy()

    def npcDestroy(self):
        if self.npc:
            if self.npc.isSpawned(): self.npc.despawn()
            self.npc.destroy()
            self.npc = None

    def applyEffect(self, player, effect):
        for ent in player.getNearbyEntities(15, 15, 15):
            if isinstance(ent, Player):
                ent.addPotionEffect(effect)
                ent.sendMessage('%s%s cast a spell upon you!' % (ChatColor.GOLD, player.getName()))

    def spell(self, player, right=False, book=None):
        enc = [Enchantment.ARROW_FIRE, Enchantment.ARROW_KNOCKBACK, Enchantment.ARROW_DAMAGE]
        #exp = ExperienceOrb().setExperience(20)
        #player.getWorld().spawn(player.getLocation(), exp)
        if time.time()-self.cooldown < self.ctime:
            x = self.ctime-(int(round(time.time()-self.cooldown)))
            player.sendMessage("%sSpell is still on cooldown (%ss)!" % (ChatColor.YELLOW, str(x)))
            return
        if player.getLevel() < 1: 
            return player.sendMessage('%sYou need at least 1 level to cast a spell!' % ChatColor.YELLOW)
        val = player.getLevel()-5
        if val < 0: val = 0
        player.setLevel(val)
        c = random.randint(1, 5)
        dur = random.choice([40, 60, 80, 100])*20
        if c == 1: self.applyEffect(player, PotionEffect(PotionEffectType.REGENERATION, dur, 1))
        elif c == 2: self.applyEffect(player, PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, dur, 1))
        elif c == 3: self.applyEffect(player, PotionEffect(PotionEffectType.SPEED, dur, 1))
        elif c == 4: self.applyEffect(player, PotionEffect(PotionEffectType.NIGHT_VISION, dur, 1))
        elif c == 5: self.applyEffect(player, PotionEffect(PotionEffectType.INCREASE_DAMAGE, dur, 1))
        for _ in range(1, random.randint(2, 3)):
            i = ItemStack(Material.BOW, 1)
            i.addEnchantment(Enchantment.ARROW_INFINITE, 1)
            for _ in range(1, random.randint(2, 4)):
                i.addEnchantment(random.choice(enc), random.randint(0, 1))
            player.getWorld().dropItemNaturally(player.getLocation(), i)
        self.cooldown = time.time()

    def start(self, inv, server):
        self.hashedItems = []
        if not self.hashedItems:
            for r in [(Material.COOKED_BEEF, 5), (Material.BLAZE_ROD, 1), (376, 1), (293, 1), (352, 1), (Material.COMPASS, 1)]:
                self.hashedItems.append(ItemStack(*r))
            self.hashedItems.append(self.book.dump())
        inv.addItem(*self.hashedItems)

    def getHit(self, plyr, atk, patk, pdef):
        if patk.mode == 1:
            plyr.setLevel(plyr.getLevel()+atk.getLevel())
            atk.setLevel(0)
            plyr.sendMessage('%s%s has donated their XP to you!' % (ChatColor.YELLOW, atk.getName()))
            atk.sendMessage('%sYou have given your XP to %s!' % (ChatColor.YELLOW, plyr.getName()))
            return True

    def uncloack(self):
        self.cloaked = False

    def clicky(self, obj, data, **kwargs):
        BaseDwarf.clicky(self, obj, data)
        def z(i):
            if i < 0: return 0
            return i
        p = obj.getPlayer()
        if p.getLevel() < 1 and obj.getItem().getTypeId() in [369, 376, 352, 293]:
            return p.sendMessage('%sYou must have XP to use a spell!' % ChatColor.YELLOW)
        if obj.getItem().getTypeId() == 369: #FIRE STICK (AMBIENCE_THUNDER)
            p.getWorld().playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 10, 1)
            for ent in p.getNearbyEntities(15, 15, 15):
                v = ent.getLocation().toVector().subtract(p.getLocation().toVector()).normalize()
                ent.setVelocity(v.multiply(4).setY(1))
        elif obj.getItem().getTypeId() == 376: #ARROWS
            if time.time()-self.holoc < self.holotime:
                x = self.holotime-(int(round(time.time()-self.holoc)))
                p.sendMessage("%sSpell is still on cooldown (%ss)!" % (ChatColor.YELLOW, str(x)))
                return
            self.npcDestroy()
            self.holoc = time.time()
            reg = cit.getNPCRegistry()
            self.npc = reg.createNPC(EntityType.PLAYER, '%s%s' % (self.nameplate, p.getName()))
            self.npc.spawn(p.getLocation())
            blk = p.getTargetBlock(None, 50)
            if not blk:
                return p.sendMessage('%sThats too far bro!' % ChatColor.YELLOW)
            self.npc.getNavigator().setTarget(blk.getLocation())
            self.trait = Equipment()
            self.npc.addTrait(self.trait)
            self.trait.set(1, p.getInventory().getHelmet())
            self.trait.set(2, p.getInventory().getChestplate())
            self.trait.set(3, p.getInventory().getLeggings())
            self.trait.set(4, p.getInventory().getBoots())
            doLater(self.npcDestroy, 85)

        elif obj.getItem().getTypeId() == 352: #BONE
            e = p.getWorld().spawnEntity(p.getLocation().add(0, 1, 0), bukkit.entity.EntityType.PRIMED_TNT)
            v = Vector().copy(p.getEyeLocation().getDirection()).setY(0).normalize()
            v.multiply(1).setY(.45)
            e.setVelocity(v)
            return p.setLevel(p.getLevel()-2)
        elif obj.getItem().getTypeId() == 293:
            if self.cloaked: return p.sendMessage('%sYour already cloaked!' % ChatColor.YELLOW)
            p.addPotionEffect(PotionEffect(PotionEffectType.INVISIBILITY, 600, 1))
            doLater(self.uncloack, 30)
            self.cloaked = True
            p.sendMessage("%sYou've been cloaked!" % ChatColor.YELLOW)
            return p.setLevel(p.getLevel()-5)
        else: return
        p.setLevel(p.getLevel()-1)
