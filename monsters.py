# -*- coding: utf-8 -*-
from org.bukkit.event.block import Action
from org.bukkit.inventory import ItemStack
from org.bukkit.potion import PotionEffect, PotionEffectType
from org.bukkit.enchantments import Enchantment
from org.bukkit import Material, ChatColor, Effect, Sound
from org.bukkit.util import Vector
import org.bukkit as bukkit
import random, time, thread
from etc import clean as c

mobapi = None
mobdis = None
game = None
server = bukkit.Bukkit.getServer()

def generatePortal(loc):
    loc = loc.getWorld().getHighestBlockAt(loc).getLocation()
    game.portalLocation = loc
    loc.add(0, 12, 0)
    loc.getWorld().createExplosion(game.portalLocation, 10, False)
    game.portalSpawn = game.portalLocation.clone()
    game.portalSpawn.add(0, 2, 0)

    s = [loc.getX(), loc.getY(), loc.getZ()]
    for x in [-2, -1, 0, 1, 2]:
        for z in [-2, 0, -1, 1, 2]:
            i = bukkit.Location(loc.getWorld(), int(s[0]+x), int(s[1]), int(s[2]+z))
            i.getBlock().setTypeId(121)
    loc.getBlock().setTypeId(130)

class BaseMonster(object):
    def __init__(self, **kwargs):
        self.name = None
        self.hashedItems = None
        self.hashedArmor = None
        self.super = False
        self.cooldownA = 0
        self.ctimeA = 0
        self.cooldownB = 0
        self.ctimeB = 0

        self.implementsLevel = False
        self.teleported = False

        self.__dict__.update(kwargs)

    def disguise(self, plyr): pass
    def start(self, plyr): pass
    def actionThrow(self, plyr, obj): pass
    def takeDamage(self, plyr, obj): pass
    def actionUse(self, plyr, obj): pass
    def actionPunch(self, plyr, obj): pass
    def enderTP(self, plyr):
        if self.teleported:
            return c("You've already teleported once!", plyr.sendMessage)
        self.teleported = True
        plyr.addPotionEffect(PotionEffect(PotionEffectType.CONFUSION, 150, 10))
        time.sleep(5)
        plyr.teleport(game.portalSpawn)
        c("You've been teleported!", plyr.sendMessage)

class Zombie(BaseMonster):
    def __init__(self):
        BaseMonster.__init__(self, name="zombie")

    def disguisep(self, plyr):
        self.disguise = mobdis(mobapi.newEntityID(), mobdis.MobType.Zombie)
        mobapi.disguisePlayer(plyr, self.disguise)

    def start(self, plyr):
        self.disguisep(plyr)
        plyr.addPotionEffect(PotionEffect(PotionEffectType.SLOW, 999999, 1))
        inv = plyr.getInventory()

        if not self.hashedItems:
            i = []
            for r in [(Material.IRON_SWORD, 1), (Material.COOKED_BEEF, 64), (345, 1)]:
                i.append(ItemStack(*r))
            self.hashedItems = i

        if not self.hashedArmor:
            arm = [ItemStack(f, 1) for f in [(313), (312), (311)]]
            for i in arm:
                i.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 3)
            self.hashedArmor = arm

        inv.setArmorContents(self.hashedArmor)
        inv.addItem(*self.hashedItems)

class Creeper(BaseMonster):
    def __init__(self):
        BaseMonster.__init__(self, name="creeper")

    def disguisep(self, plyr):
        self.disguise = mobdis(mobapi.newEntityID(), mobdis.MobType.Creeper)
        mobapi.disguisePlayer(plyr, self.disguise)

    def start(self, plyr):
        self.disguisep(plyr)
        plyr.addPotionEffect(PotionEffect(PotionEffectType.SPEED, 999999, 0))  #FOR ZE LULZ
        inv = plyr.getInventory()
        if not self.hashedItems:
            i = []
            for r in [(Material.SULPHUR, 1), (Material.COOKED_BEEF, 5), (Material.COMPASS, 1)]:
                i.append(ItemStack(*r))
            self.hashedItems = i
        inv.setArmorContents([ItemStack(*f) for f in [(301, 1), (300, 1), (299, 1), (298, 1)]])
        inv.addItem(*self.hashedItems)

    def actionUse(self, plyr, obj):
        if obj.getItem().getTypeId() == 289:
            plyr.getWorld().createExplosion(plyr.getLocation(), 10 if self.super else 4.5, False)

class Skeleton(BaseMonster):
    def __init__(self):
        BaseMonster.__init__(self, name="skeleton")
        self.snows = 0

    def disguisep(self, plyr):
        self.disguise = mobdis(mobapi.newEntityID(), mobdis.MobType.Skeleton)
        mobapi.disguisePlayer(plyr, self.disguise)

    def start(self, plyr):
        self.disguisep(plyr)
        inv = plyr.getInventory()
        if not self.hashedItems:
            i = []
            for r in [(Material.COOKED_BEEF, 5), (Material.COMPASS, 1), (Material.ARROW, 1), (Material.SNOW_BALL, 3)]:
                i.append(ItemStack(*r))
            p = ItemStack(Material.BOW, 1)
            p.addEnchantment(Enchantment.ARROW_DAMAGE, random.randint(1, 3))
            p.addEnchantment(Enchantment.ARROW_INFINITE, 1)
            if random.randint(1, 2) == 2:
                p.addEnchantment(Enchantment.ARROW_KNOCKBACK, 1)
            if random.randint(1, 2) == 2:
                p.addEnchantment(Enchantment.ARROW_FIRE, 1)
            i.append(p)
            self.hashedItems = i
        if not self.hashedArmor:
            i = [ItemStack(*f) for f in [(301, 1), (300, 1), (299, 1), (298, 1)]]
            for item in i:
                item.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4)
        inv.setArmorContents(self.hashedArmor)
        inv.addItem(*self.hashedItems)

    def actionThrow(self, plyr, obj):
        self.snows += 1
        if self.snows > 3:
            plyr.addPotionEffect(PotionEffect(PotionEffectType.CONFUSION, 100, 10))
            return plyr.sendMessage('%sNice try neek, better luck next time. LOLOLOLOLOL' % ChatColor.RED) #Because fuck you neek
        plyr.getWorld().createExplosion(obj.getLocation(), 3.7, False)

class Kitty(BaseMonster):
    def __init__(self):
        BaseMonster.__init__(self, name="kitty")
        self.defused = False

    def bombLoop(self, plyr, loc, t): #WOOD_CLICK
        plyr.setLevel(t)
        for i in range(0, t):
            if self.defused: return
            plyr.setLevel(plyr.getLevel()-1)
            loc.getWorld().playSound(loc, Sound.WOOD_CLICK, 2, 1)
            time.sleep(1)
        plyr.getWorld().createExplosion(loc, 10, False)

    def disguisep(self, plyr):
        self.disguise = mobdis(mobapi.newEntityID(), 'baby', mobdis.MobType.Ocelot)
        mobapi.disguisePlayer(plyr, self.disguise)

    def start(self, plyr):
        self.disguisep(plyr)
        inv = plyr.getInventory()
        if not self.hashedItems:
            i = []
            for r in [(Material.COOKED_BEEF, 5), (Material.COMPASS, 1), (Material.MAGMA_CREAM, 1), (390, 1)]:
                i.append(ItemStack(*r))
            self.hashedItems = i
        if not self.hashedArmor:
            i = [ItemStack(*f) for f in [(301, 1), (300, 1), (299, 1), (298, 1)]]
            for item in i:
                item.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4)
        inv.setArmorContents(self.hashedArmor)
        inv.addItem(*self.hashedItems)

    def actionPunch(self, plyr, obj):
        if time.time()-self.cooldownA >= self.ctimeB:
            self.cooldownA = time.time()
            if mobapi.isDisguised(plyr):
                mobapi.undisguisePlayer(plyr)
            mobapi.disguisePlayer(plyr, mobdis(mobapi.newEntityID(), obj.getName(), None))
            c('Disguised as %s!' % obj.getName())
        else:
            x = float(self.ctimeA)-round(time.time()-self.cooldownA)
            c("Action is still on cooldown (%ss)!" % x, plyr.sendMessage)
        return True

class Spider(BaseMonster):
    def __init__(self):
        BaseMonster.__init__(self, name="spider", ctimeB=1, ctimeA=3.5)

    def disguisep(self, plyr):
        self.disguise = mobdis(mobapi.newEntityID(), mobdis.MobType.Spider)
        mobapi.disguisePlayer(plyr, self.disguise)

    def start(self, plyr):
        self.disguisep(plyr)
        plyr.addPotionEffect(PotionEffect(PotionEffectType.JUMP, 999999, 1))
        plyr.addPotionEffect(PotionEffect(PotionEffectType.SPEED, 999999, 2))
        inv = plyr.getInventory()
        if not self.hashedItems:
            i = []
            for r in [(Material.MAGMA_CREAM, 1), (Material.COOKED_BEEF, 5), (Material.COMPASS, 1), (30, 3)]:
                i.append(ItemStack(*r))
            self.hashedItems = i
        if not self.hashedArmor:
            d = ItemStack(309, 1)  #feet
            d.addEnchantment(Enchantment.PROTECTION_FALL, 4)
            self.hashedArmor = d
        inv.setBoots(self.hashedArmor)
        inv.addItem(*self.hashedItems)

    def actionUse(self, plyr, obj):
        if obj.getItem().getTypeId() == 378:
            if time.time()-self.cooldownA >= self.ctimeA:
                self.cooldownA = time.time()
                new = Vector()
                if obj.getAction() in [Action.LEFT_CLICK_BLOCK,  Action.LEFT_CLICK_AIR]:
                    heading = plyr.getEyeLocation().getDirection()
                    new.copy(heading).setY(0).normalize()
                    new.multiply(3.5).setY(1)
                else:
                    new.setY(2.5)
                plyr.setVelocity(new)
                c("Wooooosh!", plyr.sendMessage)
            else:
                x = float(self.ctimeA)-round(time.time()-self.cooldownA)
                c("Action is still on cooldown (%ss)!" % x, plyr.sendMessage)

    def actionPunch(self, plyr, obj):
        if time.time()-self.cooldownB >= self.ctimeB:
            self.cooldownB = time.time()
            x = random.randint(1, 3)
            if x == 1: obj.addPotionEffect(PotionEffect(PotionEffectType.POISON, random.randint(100, 700), random.randint(1, 3)))
            elif x == 2: obj.addPotionEffect(PotionEffect(PotionEffectType.CONFUSION, random.randint(100, 700), random.randint(1, 3)))
            else: obj.addPotionEffect(PotionEffect(PotionEffectType.BLINDNESS, random.randint(100, 700), random.randint(1, 3)))
        return True

class Pigman(BaseMonster):
    def __init__(self):
        BaseMonster.__init__(self, name="pigman")

    def disguisep(self, plyr):
        self.disguise = mobdis(mobapi.newEntityID(), mobdis.MobType.PigZombie)
        mobapi.disguisePlayer(plyr, self.disguise)

    def start(self, plyr):
        self.disguisep(plyr)
        plyr.addPotionEffect(PotionEffect(PotionEffectType.SLOW, 999999, 0))
        plyr.addPotionEffect(PotionEffect(PotionEffectType.FIRE_RESISTANCE, 999999, 1))
        inv = plyr.getInventory()
        if not self.hashedItems:
            i = []
            for r in [(Material.COOKED_BEEF, 5), (Material.COMPASS, 1), (Material.LAVA_BUCKET, 1)]:
                i.append(ItemStack(*r))
            it = ItemStack(Material.DIAMOND_SWORD, 1)
            it.addEnchantment(Enchantment.KNOCKBACK, 1)
            it.addEnchantment(Enchantment.FIRE_ASPECT, 1)
            i.append(it)
            self.hashedItems = i

        if not self.hashedArmor:
            arm = [ItemStack(*f) for f in [(309, 1), (308, 1), (307, 1), (306, 1)]]
            for i in arm:
                i.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 3)
            self.hashedArmor = arm

        inv.setArmorContents(self.hashedArmor)
        inv.addItem(*self.hashedItems)

    def actionUse(self, plyr, obj): pass

class Enderman(BaseMonster):
    def __init__(self):
        BaseMonster.__init__(self, name="enderman", ctimeA=30)
        self._cancan = False
        self.disabled = False
        self.implementsLevel = True
        #self.loc = None

    def disguisep(self, plyr):
        self.disguise = mobdis(mobapi.newEntityID(), mobdis.MobType.Enderman)
        mobapi.disguisePlayer(plyr, self.disguise)

    def start(self, plyr):
        self.disguisep(plyr)
        inv = plyr.getInventory()
        if not self.hashedItems:
            i = []
            for r in [(Material.COOKED_BEEF, 10), (Material.COMPASS, 1), (Material.SNOW_BALL, 1), (358, 1)]:
                i.append(ItemStack(*r))
            self.hashedItems = i

        if not self.hashedArmor:
            arm = [ItemStack(*f) for f in [(309, 1), (308, 1), (307, 1), (306, 1)]]
            for i in arm:
                i.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 3)
            self.hashedArmor = arm

        inv.setArmorContents(self.hashedArmor)
        inv.addItem(*self.hashedItems)

    def isCooldown(self):
        if time.time()-self.cooldownA >= .8:
            return False
        return True

    def placePortalAction(self, p):
        c("Attempting to create portal!", p.sendMessage)
        self._cancan = True
        p.setLevel(20)
        for i in range(1, 21):
            p.setLevel(p.getLevel()-1)
            time.sleep(1)
            if not self._cancan or game.hasPortal:
                p.setLevel(0)
                return c("Portal Creation %($4)sFailed%($e)s!", p.sendMessage)
        generatePortal(p.getLocation())
        game.hasPortal = True
        c("Created portal successfully!", p.sendMessage)
        c("%($4)sA PORTAL HAS BEEN CREATED!!")
        c("%($4)sLeft click your compass to teleport!", game.sendToMobs)
        self.disabled = True

    def takeDamage(self, plyr, obj):
        self._cancan = False

    def actionUse(self, plyr, obj):  #Use XP for "loading"
        if self.disabled and self._cancan or game.hasPortal: return
        if obj.getItem().getTypeId() == 358:
            thread.start_new_thread(self.placePortalAction, (plyr,))

    def actionThrow(self, plyr, obj):
        plyr.getInventory().addItem(ItemStack(Material.SNOW_BALL, 1))
        if not self.isCooldown():  #not needed?
            self.cooldownA = time.time()
            plyr.setFallDistance(0)
            loc = obj.getLocation().clone()
            loc.setPitch(plyr.getLocation().getPitch())
            loc.setYaw(plyr.getLocation().getYaw())
            plyr.teleport(loc)
            for i in range(1, 9):  #see: http://bit.ly/NtREIP / http://mc.kev009.com/Protocol
                loc.getWorld().playEffect(loc, Effect.SMOKE, i, 3)

def setGlobal(s, g):
    global server, game
    server = s
    game = g

def monsterSetMd(api, dis):
    global mobapi, mobdis
    mobapi = api
    mobdis = dis
