# -*- coding: utf-8 -*-
import org.bukkit as bukkit
from net.minecraft.server import Packet20NamedEntitySpawn, Packet5EntityEquipment
from net.minecraft.server import NBTBase, NBTTagCompound, NBTTagList, NBTTagString
from org.bukkit import ChatColor, Material
from org.bukkit.inventory import ItemStack
from org.bukkit.craftbukkit.inventory import CraftItemStack
from org.bukkit.potion import PotionEffect, PotionEffectType
from monsters import *
from dwarves import *
from etc import clean as c
from org.bukkit import ChatColor as CC
import random, time

mobapi = None

dwarf_starts = {
    'Builder':Builder,
    'Miner':Miner,
    'Baker':Baker,
    'Blacksmith':BlackSmith,
    'Alchemist':Alchemist,
    'Mage':Mage,
}

mobs = {
    'Zombie':Zombie,
    'Creeper':Creeper,
    'Skeleton':Skeleton,
    'Spider':Spider,
    'Enderman':Enderman,
    'Pigman':Pigman,
    'Kitty':Kitty
}

classes = {
    'builder':None,
}

def single(plyr, ent):
    plyr.getHandle().netServerHandler.sendPacket(Packet20NamedEntitySpawn(ent))

def addHead(plyr, pak):
    for p in bukkit.Bukkit.getOnlinePlayers():
        if p != plyr:
            p.getHandle().netServerHandler.sendPacket(pak)

def refresh(plyr, ent):
    for p in bukkit.Bukkit.getOnlinePlayers():
        if p != plyr:
            single(p, ent)

def setName(plyr, name, sing=None):
    org = plyr.getName()
    e = plyr.getHandle()
    e.name = name
    if not sing: refresh(plyr, e)
    else: single(sing, e)
    e.name = org

class Nullzy():
    name = None

class Player():
    def __init__(self, cl, name, game):
        self.playerClass = cl #class
        self.mobClass = Nullzy() #mobs['Skeleton'] #default
        self.playerName = name
        self.game = game

        self.lastMob = None
        self.nextMob = None

        self.special = False
        self.mode = 1 #1 in game dwarf, 2 in game mob
        self.spec = False

    def getNameTag(self, plyr):
        return '%s%s' % (self.playerClass.nameplate, plyr.getName())

    def setNametag(self, plyr, **kwargs):
        setName(plyr, self.getNameTag(plyr), **kwargs)

    def setup(self, data):
        plyr = self.game.s.getPlayer(self.playerName)
        plyr.setLevel(0)
        self.clearPotions(plyr)
        if self.mode == 1:
            plyr.addPotionEffect(PotionEffect(PotionEffectType.REGENERATION, 1200, 10)) #buff the player for 1 min
            inv = plyr.getInventory()
            inv.clear()
            self.playerClass.start(inv, self.game.s)
            inv.setArmorContents([ItemStack(*f) for f in [(301, 1), (300, 1), (299, 1)]])
            helm = CraftItemStack(298, 1)
            helm.getHandle().tag = NBTTagCompound()
            helm.getHandle().tag.setCompound("display", NBTTagCompound())
            tag = helm.getHandle().tag.getCompound("display")
            tag.setInt("color", self.playerClass.color)
            helm.getHandle().tag.setCompound("display", tag)
            inv.setHelmet(helm)
            plyr.giveExp(100)
            self.setNametag(plyr)
            plyr.sendMessage('%sYou are a %s%s%s!' % (CC.GOLD, CC.AQUA, self.playerClass.name, CC.GOLD))
        elif self.mode == 2:
            #plyr = self.game.s.getPlayer(self.playerName)
            if mobapi.isDisguised(plyr):
                mobapi.undisguisePlayer(plyr)
            plyr.setCompassTarget(data['blockhash']['egg'])
            plyr.saveData()
            #plyr.setBedSpawnLocation(data['blockhash']['egg'])
            plyr.addPotionEffect(PotionEffect(PotionEffectType.REGENERATION, 200, 5)) #buff the mob for 10 secs
            plyr.addPotionEffect(PotionEffect(PotionEffectType.INCREASE_DAMAGE, 300, 5)) #buff the mob for 15
            inv = plyr.getInventory()
            inv.clear()
            self.lastMob = self.mobClass.name
            self.mobClass = self.game.getMobChoice(self) #mobs[random.choice(mobs.keys())]()
            mid = self.mobClass.start(plyr)
            plyr.sendMessage('%sYou are a %s%s%s!' % (CC.GOLD, CC.AQUA, self.mobClass.name, CC.GOLD))
            # for i in plyr.getWorld().getLivingEntities():
            #     if i.getEntityId() == mid:
            # head = CraftItemStack(Material.SKULL_ITEM, 1, 3);
            # nhead = NBTTagCompound()
            # nhead.setString("SkullOwner", plyr.getName())
            # head.getHandle().tag = nhead
            # addHead(plyr, self.mobClass.disguise.getEquipmentChangePacket(1, head))
    
    def beginBattle(self, data): #@TODO add war items
            if self.mode == 1:
                plyr = self.game.s.getPlayer(self.playerName)
                inv = plyr.getInventory()

    def clearPotions(self, plyr):
        for eff in plyr.getActivePotionEffects():
            plyr.removePotionEffect(eff.getType())

class Game():
    def __init__(self, serv):
        self.s = serv
        self.players = {}
        self.deads = []
        self.choices = [i for i in mobs]

        self.mode = 0 #0: before mobs, 1: during mobs, no destruction, 2: during mobs, enchants are gone

        self.hasPortal = False
        self.portalLocation = None #Location of the portal block
        self.portalSpawn = None  #Location of the player spawn

    def getMobChoice(self, player):
        if player.nextMob != None:
            c = player.nextMob
            player.nextMob = None
        else:
            choice = [i for i in mobs if i != player.lastMob]
            if self.hasPortal and 'Enderman' in choice: choice.pop(choice.index('Enderman'))
            c = random.choice(choice)
        return mobs[c]()

    def sendToMobs(self, msg):
        for i in self.players.values():
            if i and i.mode == 2:
                self.s.getPlayer(i.playerName).sendMessage(msg)

    def canBreakCrystal(self, obj):
        if self.mode == 2:
            if self.players[obj.getName()].mode == 2:
                return True
        return False

    def canBreakTable(self, obj):
        if self.mode != 0:
            if self.players[obj.getName()].mode == 2:
                return True
        return False

    def playerDied(self, name):
        self.deads.append(name)

    def addPlayer(self, *args):
        p = Player(*args)
        self.players[p.playerName] = p
        return p

    def getPlayer(self, name):
        if name in self.players.keys():
            return self.players[name]

    def setMode(self, mode):
        self.mode = mode

def setMd(api, dis):
    global mobapi
    mobapi = api
    monsterSetMd(api, dis)