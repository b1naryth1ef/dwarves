from org.bukkit import ChatColor

_cleankey = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'k', 'l', 'm', 'n', 'o', 'r']
_cdict = {}
for i in _cleankey:
    _cdict['$'+i] = ChatColor.getByChar(i)

def clean(text, sender=None, n=True):
    if n: text = '%($e)s'+text
    text = text % _cdict
    if text and sender:
        return sender(text)
    return text

