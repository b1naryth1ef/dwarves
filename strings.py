
strings = {
    'join_mode':{
	    "EMPTY":"%s the game.",
	    "SETUP":"%s the game.",
	    "START":"%s the pre-game.",
	    "BUILD":"%s the preperations",
	    "BATTLE":"%s the battle",
	    "OVER":'%s the game.'
    },
    'mage':["""The mage is the key protector in the battle for the core. Using your spells, you must heal, strengthen and protect your dwarven brothers. Although you are weak on the outside your powerful spells take heavy tolls on the enemies forces. Your blaze rod sends enemies (and friends) flying away, your bone throws live tnt and your diamond hoe builds walls."""],
    'builder':[""""Your job is to keep the core protected and safe from the enemey onslaught. You have only a day to work with your fellow builders, using your wooden pick and gold axe you build walls in an instant. You light the hallways of your fortress and close the holes when the forces come."""],
    'miner':["""Your job is to supply your dwarven brothers with the magical redstone dust required for their spells. You must dig the bare ground, finding stone to transform with your spell into that shiny, magical mineral. But be wary, natural minerals in the ground pose a threat towards your survival, and have been known to explode."""],
    'baker':["""Your job is to keep your dwarven brothers supplied with food and health. You take the magical redstone dust from your miner friends, and using your spell transform it into wheat, coal and flint. Cooking the wheat and flint you get cake and golden apples to share with your dwarve brothers."""],
    'blacksmith':["""Your job as the blacksmith is key to the dwarfs survival. Your secret spell transforms redstone dust into coal, red mushrooms and arrows. Cooking the mushrooms you get diamonds to build armor and tools for your brothers."""],
    'alchemist':["""Your job is to brew potions of various effects, impowering your dwarven brotherhood to unimaginable speeds and strengths. Taking redstone from the miners, you will turn the dust into magical ingriedents which only you shall know how to brew with."""],
}



