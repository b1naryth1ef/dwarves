# -*- coding: utf-8 -*-
__plugin_name__ = "Dwarf"
__plugin_version__ = "0.2pre"
__plugin_author__ = "B1nzy"

import time, random, thread
from strings import strings
from rawr import dwarf_starts, Game, setMd, Nullzy, mobs
from etc import clean as c
from monsters import BaseMonster, Enderman, setGlobal
from dwarves import BaseDwarf
from books import Book

import org.bukkit as bukkit
from org.bukkit.inventory import ItemStack, FurnaceRecipe
from org.bukkit.entity import EnderCrystal, Player, EntityType, Snowball, Arrow
from org.bukkit.event.block import Action
from org.bukkit import Material, GameMode, Sound, Effect
from org.bukkit import ChatColor as CC
from org.bukkit.event.inventory import CraftItemEvent 
from org.bukkit.event.entity.EntityDamageEvent import DamageCause
from org.bukkit.potion import PotionEffect, PotionEffectType
from org.bukkit.event.inventory.InventoryType import SlotType

#Modes
#0 - No game
#1 - Waiting to setup
#2 - Waiting to start
#3 - Active building
#4 - Active in battle
#5 - over

server = bukkit.Bukkit.getServer()
g = Game(server)

setGlobal(server, g)

commands = {}
snowball = []
bombs = {}
default_data = {
    'mode': "EMPTY",
    'blockhash': {
        'egg': None,
        'tables': [],
    },
    'classes': {
    1: []
    },
    'class_req': ["Builder", "Blacksmith", "Baker", "Builder"]  #["Miner","Blacksmith", "Builder", ] #Spawn these classes first.
}
data = default_data.copy()
random.shuffle(data['classes'][1])

def doLater(action, sleep, *args, **kwargs):
    t = time.time()
    while time.time()-t < sleep:
        time.sleep(0.2)
    return action(*args, **kwargs)

def cmd(name):
    def deco(func):
        commands[name] = func
        return func
    return deco

# Switch modes after a period of time (or on force)
def timeReady():
    server.broadcastMessage("%sBATTLE MODE!" % (CC.RED))
    data['mode'] = "BATTLE"
    mobs = [i for i in g.players.values() if i.mode == 2]
    if not len(mobs):
        p = g.players[random.choice(g.players.keys())]
        p = server.getPlayer(p.playerName)
        if p:
            p.sendMessage("%sYou've been struck ill! If only there was a cure..." % (CC.RED))
            p.addPotionEffect(PotionEffect(PotionEffectType.WITHER, 5000, 1))
            p.addPotionEffect(PotionEffect(PotionEffectType.CONFUSION, 5000, 10))
            #doLater(kill, 30, p) #I dont think we need this...
    for po in g.players.values():
        if po and po.mode == 2:
            p = server.getPlayer(po.playerName)
            if p: p.setHealth(0)

# Clear the area the core needs to sit
def clearSpawn(seed):
    world = seed.getWorld()
    pos = (seed.getX(), seed.getY(), seed.getZ())
    ref = [-2, -1, 0, 1, 2]

    #clear 5x5x5 area
    for A in ref:
        for B in range(-1, 5):
            for C in ref:
                if (A, B, C) == (0, 0, 0): continue
                bukkit.Location(world, pos[0]+A, pos[1]+B, pos[2]+C).getBlock().setTypeId(0)

# Generate the core
def renderSpawn(seed):
    pz = []

    # Bottom layer (bedrock)
    for i in [-1, -2, 0, 1, 2]:
        for f in [-1, -2, 0, 1, 2]:
            pz.append([bukkit.Location(seed.getWorld(), int(seed.getX()+f), int(seed.getY()-1), int(seed.getZ()+i)), 7])

    # Enchanting Tables
    for i in [-1, 1]:
        for f in [-1, 1]:
            lz = bukkit.Location(seed.getWorld(), int(seed.getX()+f), int(seed.getY()), int(seed.getZ()+i))
            pz.append([lz, 116])
            data['blockhash']['tables'].append(lz)

    #Dragon egg
    pz.append([seed, 122])
    return pz

# Choose a dwarf class
def generateChoice(p):  #1: dwarf start | 2: dwarf battle | 3: mob
    if len(data['class_req']): cl = data['class_req'].pop()
    else: cl = random.choice(data['classes'][1])
    return dwarf_starts[cl]

# Does the enderman need to be injured or the portal-creation cancelled?
def endermanCheck(p, obj, l):
    if l.getBlock().getTypeId() in (8, 9):
        obj.getPlayer().damage(2)
    if p.mobClass._cancan:
        f = obj.getFrom()
        t = obj.getTo()
        if not f.getX() == t.getX() or not f.getY() == t.getY() or not f.getZ() == t.getZ():
            p.mobClass._cancan = False

@cmd('new')
def newGame(sender, cmd, args):
    """Create a new game"""
    sender.getWorld().setWeatherDuration(0)
    if data['mode'] != "EMPTY":
        sender.sendMessage('%sAn active game exists! Please delete before creating a new one.' % CC.YELLOW)
    else:
        data['mode'] = "SETUP"
        sender.sendMessage('%sNew game created! Waiting to setup...' % CC.YELLOW)

@cmd('me')
def me(sender, cmd, args):
    if len(args) < 1:
        sender.sendMessage('%sMust supply a dwarf class name!' % CC.YELLOW)
        return True
    a = args[1].strip().lower()
    if a.title() not in dwarf_starts.keys():
        sender.sendMessage('%sInvalid dwarf class!' % CC.YELLOW)
        return True
    if len(args) == 3:
        sender = server.getPlayer(args[2])
    if not sender: return
    g.addPlayer(dwarf_starts[a.title()](), sender.getName(), g)
    sender.sendMessage('%sYou will be a %s%s%s when you spawn!' % (CC.YELLOW, CC.RED, a, CC.YELLOW))
    return True

@cmd('del')
def delGame(sender, cmd, args):
    """Delete an active game"""
    global data
    if data['mode'] == "EMPTY":
        return sender.sendMessage('%sNo game exists!' % CC.YELLOW)
    if data['mode'] == "BATTLE":
        if server.getScheduler().isQueued(data['delay_pid']):
            server.getScheduler().cancelTask(data['delay_pid'])
    data = {}
    data = default_data.copy()  #This resets the data to our default dict
    sender.sendMessage('%sDeleted current game!' % CC.YELLOW)

def setupPlayer(plyr):
    plyr.setHealth(plyr.getMaxHealth())
    plyr.setFoodLevel(20)
    plyr.teleport(data['dwarfspawn'])  #@TODO We may need to send a packet for this w/ diguise craft
    p = g.getPlayer(plyr.getName())
    if not p:
        p = g.addPlayer(generateChoice(1)(), plyr.getName(), g)
    p.setup(data)

@cmd('start')
def startGame(sender, cmd, args):
    """Start a ready game"""
    if data['mode'] != "START":
        sender.sendMessage('%sNot waiting to start! (Have you created the core?)' % CC.YELLOW)
    else:
        # Set time, start time thread
        sender.getWorld().setTime(0)
        data['delay_pid'] = server.getScheduler().scheduleSyncDelayedTask(pyplugin, timeReady, 2000*20)  # *20 = ticks | 1200 - one cycle | 2400 - 2 days

        # Set mode, broadcast
        data['mode'] = "BUILD"
        server.broadcastMessage('%sGAME IS STARTING!' % CC.RED)
        server.broadcastMessage('%sBUILD MODE!' % CC.GOLD)

        # Spawn players
        li = server.getOnlinePlayers()
        random.shuffle(li)
        for plyr in li:
            plyr.setGameMode(GameMode.SURVIVAL)
            setupPlayer(plyr)

@cmd('cm')
def createMon(sender, cmd, args):
    """Create the main monument"""
    if data['mode'] != "SETUP":
        sender.sendMessage('%sNo new game waiting for monument!' % CC.YELLOW)
    else:
        b = sender.getTargetBlock(None, 100)
        b.setTypeId(0)
        lc = b.getLocation()
        data['blockhash']['egg'] = lc
        data['dwarfspawn'] = lc.clone()
        data['dwarfspawn'].add(0, 4, 0)
        clearSpawn(b.getLocation())
        for block in renderSpawn(lc):
            block[0].getBlock().setTypeId(block[1])
        sender.sendMessage('%sMonument created! Ready for origin point!' % CC.YELLOW)

@cmd('ms')
def createMob(sender, cmd, args):
    """Set the mob spawn location"""
    if data['mode'] != "SETUP":
        sender.sendMessage('%sNo new game waiting for mob spawn!' % CC.YELLOW)
    elif "origin" not in data.keys():
        sender.sendMessage('%sSet the origin point first!' % CC.YELLOW)
    else:
        data['mobspawn'] = sender.getTargetBlock(None, 100).getLocation()
        data['mode'] = "START"
        sender.sendMessage('%sMob spawn set! Ready to start!' % CC.YELLOW)

@cmd('org')
def setOrigin(sender, cmd, args):
    """Set the main spawn location (obsidian box plz!)"""
    if data['mode'] != "SETUP":
        sender.sendMessage('%sNo new game waiting for origin point!' % CC.YELLOW)
    else:
        data['origin'] = sender.getTargetBlock(None, 100).getLocation()
        sender.getWorld().setSpawnLocation(int(data['origin'].getX()), int(data['origin'].getY()), int(data['origin'].getZ()))
        sender.sendMessage('%sOrigin point set! Ready for mob spawn...' % CC.YELLOW)

@cmd('about')
def about(sender, cmd, args):
    sender.sendMessage('%sDwarf Bukkit Mod Info:' % CC.YELLOW)
    sender.sendMessage('%sVersion:%s %s' % (CC.YELLOW, CC.RED, __plugin_version__))
    sender.sendMessage('%sAuthor:%s %s' % (CC.YELLOW, CC.RED, __plugin_author__))

@cmd('choice')
def about(sender, cmd, args):
    p = server.getPlayer(sender.getName())
    po = g.getPlayer(p.getName())
    if p and po:
        if po.mode == 2 and len(args) >= 2:
            i = args[1].lower().title()
            if i in mobs.keys():
                po.nextMob = i
                p.sendMessage('%sYou will be a %s%s%s next round!' % (CC.YELLOW, CC.AQUA, i, CC.YELLOW))
        else:
            return False
    return True

@hook.enable
def onEnable():
    global log
    log = pyplugin.getLogger()
    log.info('Enabling Dwarf v%s' % info.getVersion())
    server.addRecipe(FurnaceRecipe(ItemStack(Material.CAKE_BLOCK), Material.WHEAT))
    server.addRecipe(FurnaceRecipe(ItemStack(Material.DIAMOND), Material.RED_MUSHROOM))
    server.addRecipe(FurnaceRecipe(ItemStack(322, 1, 1), Material.FLINT))

@hook.disable
def onDisable():
    log.info("Dwarf v%s has been Disabled!" % info.getVersion())

@hook.command("dwarf", usage="/<command>", desc="Manage/Create a Dwarf game")
def dwarfCommand(sender, cmd, lbl, args):
    if not sender.isOp(): return True
    if len(args) >= 1:
        if args[0] in commands:
            commands[args[0]](sender, cmd, args[0:])
            return True
        else:
            sender.sendMessage('%sUnknown command %s' % (CC.RED, args[0]))
    sender.sendMessage('%sList of dwarvian commands:' % CC.YELLOW)
    for i in commands:
        sender.sendMessage('%s/%s %s: %s%s' % (CC.DARK_RED, cmd.getLabel(), i, commands[i].__doc__ or "no description", CC.YELLOW)) 
    return True

@hook.command('test', usage="/<command>", desc="Testing command")
def testCommand(sender, cmd, lbl, args):  # This allows us to skip the wait period
    data['mode'] = "BATTLE"
    server.getScheduler().cancelTask(data['delay_pid'])
    timeReady()
    return True

# Custom join text
@hook.event('player.PlayerJoinEvent', 'normal')
def playerJoinListener(obj):
    p = obj.getPlayer()
    p.getInventory().clear()
    for eff in p.getActivePotionEffects():
        p.removePotionEffect(eff.getType())
    if p.isOp(): p.setGameMode(GameMode.CREATIVE)
    if data['mode'] in ['EMPTY', 'SETUP'] and not p.isOp():
        p.kickPlayer('%sThe server is being setup, please be patient.' % CC.DARK_RED)
    c("%($6)s"+p.getDisplayName()+"%($6)s "+strings['join_mode'][data['mode']] % "joined", obj.setJoinMessage)
    if data['mode'] in ["BUILD", "BATTLE"]:
        po = g.getPlayer(p.getName())
        if po:
            for ply in g.players.values(): #Refresh nameplates
                ply.setNameTag(server.getPlayer(ply.playerName), sing=p)
            if po.mode == 2: po.mobClass.disguise(p)
            elif po.mode == 1: po.setNametag(p)
            return
        setupPlayer(p)
        if data['mode'] == "BATTLE":
            p.setHealth(0)  #kill the player, like a douche!

# Custom part text
@hook.event('player.PlayerQuitEvent', 'normal')
def playerQuitListener(obj):
    p = obj.getPlayer()
    c("%($6)s"+p.getDisplayName()+"%($6)s "+strings['join_mode'][data['mode']] % "quit", obj.setQuitMessage)

# Hook actionPunch, takeDamage and getHit
@hook.event('entity.EntityDamageByEntityEvent', 'normal')
def entityDamageListener(obj):
    ent = obj.getEntity()
    if isinstance(ent, Player):
        if data['mode'] not in ["START", "SETUP"]:
            if obj.getCause() == DamageCause.ENTITY_ATTACK: #Call actionPunch()
                if isinstance(obj.getDamager(), Player):
                    po = g.getPlayer(obj.getDamager().getName())
                    if po and po.mode == 1:
                        if po.mobClass.name == 'mage' and po.mobClass.cloaked:
                            obj.setCancelled(True)
                    if po and po.mode == 2:
                        if not isinstance(po.mobClass, Nullzy):
                            if po.mobClass.actionPunch(obj.getDamager(), obj.getEntity()):
                                obj.setCancelled(True)
                if isinstance(obj.getEntity(), Player): #Call takeDamage()
                    po = g.getPlayer(obj.getEntity().getName())
                    if po and po.mode == 2:
                        if not isinstance(po.mobClass, Nullzy):
                            if po.mobClass.takeDamage(obj.getEntity(), obj.getDamager()):
                                obj.setCancelled(True)
                if isinstance(obj.getEntity(), Player) and isinstance(obj.getDamager(), Player): #Call getHit()
                    if obj.getDamager().getItemInHand().getTypeId() == 387:
                        patk = g.getPlayer(obj.getDamager().getName())
                        pdef = g.getPlayer(obj.getEntity().getName())
                        if pdef and pdef.playerClass.implementsHit:
                            if pdef.playerClass.getHit(obj.getEntity(), obj.getDamager(), patk, pdef):
                                obj.setCancelled(True)

# Spiders/enderman/hook takeDamage
@hook.event('entity.EntityDamageEvent')
def entityDamageNormListener(obj):
    ent = obj.getEntity()
    if isinstance(ent, Player):
        if data['mode'] not in ["START", "SETUP"]:
            po = g.getPlayer(obj.getEntity().getName())
            if obj.getCause() == DamageCause.FALL: #No fall damage for spiders
                if po and po.mode == 2 and not isinstance(po.mobClass, Nullzy):
                    if po.mobClass.name == "spider":
                        obj.setCancelled(True)
            if po and po.mode == 2: #Hook takeDamage
                if not isinstance(po.mobClass, Nullzy):
                    if po.mobClass.takeDamage(obj.getEntity(), None):
                        obj.setCancelled(True)
            if po and po.mode == 2: #Enderman checking
                if isinstance(po.mobClass, Enderman) and hasattr(obj, 'getLocation'):
                    endermanCheck(po, obj, obj.getLocation())

# Core-protection, Etc
@hook.event('block.BlockBreakEvent', 'normal')
def blockBreakListener(obj):
    if obj.getBlock().getLocation() in data['blockhash']['tables']:
        if g.getPlayer(obj.getPlayer().getName()).mode == 2:
            if data['mode'] not in ["START", "SETUP"]:
                data['blockhash']['tables'].pop(data['blockhash']['tables'].index(obj.getBlock().getLocation()))
                if len(data['blockhash']['tables']) == 0:
                    g.setMode(2)
                    server.broadcastMessage('%sCORE IS UNPROTECTED!' % CC.DARK_RED)
                    return
                else:
                    msg = "%sCORE PROTECTION IS AT %s%s" % (CC.YELLOW, CC.RED, str(len(data['blockhash']['tables'])*25))
                    server.broadcastMessage(msg)
                    return
            obj.setCancelled(True)
        else: obj.setCancelled(True)
    elif obj.getBlock().getTypeId() == 140:
        loc = obj.getBlock().getLocation()
        l = (loc.getX(), loc.getY(), loc.getZ())
        if po.mode != 1: return obj.setCancelled(True)
        if l in bombs.keys():
            po = g.getPlayer(bombs[l])
            po.mobClass.defused = True
            p = server.getPlayer(po.playerName)
            p.sendMessage('%sYour bomb has been %sdefused%s!' % (CC.YELLOW, CC.RED, CC.YELLOW))
            p.setHealth(0)
            server.broadcastMessage('%s%s has defused a bomb!' % (CC.YELLOW, p.getDisplayName()))
    elif obj.getBlock().getTypeId() in [14, 15, 16, 21, 56, 73, 73]: #Explode on finding ore!
        if random.randint(1, 5) == 3:
            obj.getBlock().getLocation().getWorld().createExplosion(obj.getBlock().getLocation(), 4.5, False)
            obj.setCancelled(True)
    elif obj.getBlock().getLocation() == data['blockhash']['egg']:
        obj.setCancelled(True)
    elif obj.getBlock().getTypeId() == 324 and g.getPlayer(obj.getPlayer().getName()).mode == 2:
        obj.setCancelled(True) # Mobs cant break doors
    elif g.hasPortal and g.portalLocation:
        if obj.getBlock().getLocation() == g.portalLocation:
            if g.getPlayer(obj.getPlayer().getName()).mode == 1:
                g.hasPortal = False
                g.portalLocation = None
                g.portalSpawn = None
                server.broadcastMessage('%sThe monsters portal has been broken by %s' % (CC.RED, obj.getPlayer().getName()))
            else: obj.setCancelled(True)

# Ignore weather (for now...)
@hook.event('weather.WeatherChangeEvent')
def weatherListener(obj):
    if obj.toWeatherState() != False:
        obj.setCancelled(True)

# Dwarf >> Mob
@hook.event('entity.PlayerDeathEvent')
def playerDeathListener(obj):
    if isinstance(obj.getEntity(), Player):
        p = obj.getEntity()
        po = g.getPlayer(p.getName())
        # Make a dwarf a monster when they die
        if po and po.mode == 1:
            po.playerClass.die(p)
            obj.setDeathMessage('%s%s %shas fallen to the monsters...' % (CC.GOLD, p.getName(), CC.RED))
            po.mode = 2
            g.playerDied(p.getName())
        # Monsters dont drop items
        elif po and po.mode == 2:
            obj.getDrops().clear()

# Prevent dwarves from dropping spell-books
@hook.event('player.PlayerDropItemEvent')
def playerDropListener(obj):
    p = obj.getPlayer()
    pg = g.getPlayer(p.getName())
    if pg and not pg.mode == 2:
        if obj.getItemDrop().getItemStack() == ItemStack(387, 1):
            obj.setCancelled(True)
            p.sendMessage("%sYou cant drop you spell book! You'll need that!" % (CC.RED))

# Setup players on respawn if they are a mob
@hook.event('player.PlayerRespawnEvent')
def playerRespawnListener(obj):
    p = obj.getPlayer()
    po = g.getPlayer(p.getName())
    if po:
        if po.mode == 2:
            if data['mode'] == "BATTLE":
                obj.setRespawnLocation(data['mobspawn'])
                thread.start_new_thread(doLater, (po.setup, .5, data))
                return
    if data['mode'] not in ["START", "EMPTY", "OVER"]:
        obj.setRespawnLocation(data['origin'])

_rkey = {
    Action.LEFT_CLICK_BLOCK:False,
    Action.RIGHT_CLICK_BLOCK:True,
    Action.LEFT_CLICK_AIR:False,
    Action.RIGHT_CLICK_AIR:True,
}

# Fire important events, kill the egg
@hook.event('player.PlayerInteractEvent')
def playerInteractListener(obj):
    p = g.getPlayer(obj.getPlayer().getName())
    if data['mode'] != "EMPTY":
        # Mobs cant use doors
        if obj.getClickedBlock():
            if obj.getClickedBlock().getTypeId() in [64, 324, 96, 71, 330]:
                if p and p.mode == 2 and not p.mobClass.name == 'kitty': obj.setCancelled(True)

        if obj.getItem() and p:
            if obj.getItem().getTypeId() != 387 and p.mode == 1: p.playerClass.clicky(obj, data, plug=pyplugin)
            if obj.getAction() in [Action.LEFT_CLICK_BLOCK, Action.LEFT_CLICK_AIR]:
                # Fire player spell action (book)
                if p.mode == 1:
                    if obj.getItem().getTypeId() == 387:
                       #b = Book().load(obj.getItem())
                        if True:  #@True This is meh
                            if p.playerClass.spell(obj.getPlayer(), book=None):  #right=_rkey[obj.getAction()]
                                obj.getPlayer().giveExp(random.randint(35, 55))

                # Compass action
                elif p.mode == 2:
                    if obj.getItem().getTypeId() == 345:
                        if g.hasPortal and g.portalLocation:
                            thread.start_new_thread(p.mobClass.enderTP, (obj.getPlayer(), ))

        # Egg actions etc
        if data['mode'] == "BATTLE":
            if obj.getAction() == Action.LEFT_CLICK_BLOCK:
                if obj.getClickedBlock().getLocation() == data['blockhash']['egg']:
                    if g.getPlayer(obj.getPlayer().getName()).mode == 2 and g.mode == 2:
                        obj.getPlayer().getWorld().createExplosion(obj.getPlayer().getLocation(), 30, False)
                        server.broadcastMessage('%s%s %sHAS DESTROYED THE CORE!' % (CC.AQUA, CC.RED, obj.getPlayer().getName()))
                        server.broadcastMessage('%sGAME HAS ENDED! GG ALL!' % CC.RED)
                        data['mode'] = "OVER"
            if obj.getItem() and p:
                if p and p.mode == 2 and not isinstance(p.mobClass, Nullzy):
                    p.mobClass.actionUse(obj.getPlayer(), obj)

# Prevent crafting of bread
@hook.event('inventory.CraftItemEvent')
def playerInventoryCraftEvent(obj):
    if isinstance(obj, CraftItemEvent):  #@DEV wtf is this?
        if data['mode'] != "EMPTY":
            if obj.getRecipe().getResult().getTypeId() == 297:  #crafting bread
                obj.setCancelled(True)

# This is shitty-shitty-shitty
@hook.event('server.PluginEnableEvent')  #@TODO Repair this (idk how)
def pluginListener(obj):
    global mobapi
    from pgDev.bukkit.DisguiseCraft import Disguise, DisguiseCraft
    #from pgDev.bukkit.DisguiseCraft.api import DisguiseCraftAPI
    setMd(DisguiseCraft.getAPI(), Disguise)
    print "MD: %s" % obj.getPlugin().getName()

# Dragon eggs should not teleport
@hook.event('block.BlockFromToEvent')
def blockFromToListener(obj):
    if obj.getBlock().getTypeId() == 122 and data['mode'] != "EMPTY":
        obj.setCancelled(True)

# Fix dragon eggs teleporting on ex'plodins
@hook.event('entity.EntityExplodeEvent')
def entityExplodeEventListener(obj):
    def threadme():
        for i in obj.blockList():
            if i.getTypeId() == 122 or i.getLocation() == data['blockhash']['egg']:
                i.getLocation().getBlock().setTypeId(122)
    thread.start_new_thread(threadme, ())

# Enderman should get hurt by water
@hook.event('player.PlayerMoveEvent')
def playerMoveListener(obj):  #@DEV If a player stops moving but is in water, they wont take damage?
    p = g.getPlayer(obj.getPlayer().getName())
    if p and p.mode == 2:
        if isinstance(p.mobClass, Enderman):
            endermanCheck(p, obj, obj.getTo())

# Some mobs use the level bar
@hook.event('player.PlayerExpChangeEvent')
def playerLevelChangeListener(obj):
    p = g.getPlayer(obj.getPlayer().getName())
    if p and p.mode == 2 and not isinstance(p.mobClass, Nullzy):
        if p.mobClass.implementsLevel:
            obj.setAmount(0)
    elif p and p.mode == 1:
        if p.playerClass.implementsLevel:
            obj.setAmount(0)

# Track thrown snowballs
@hook.event('entity.ProjectileLaunchEvent')
def projectileLaunchListener(obj):
    if data['mode'] == "BATTLE":
        if isinstance(obj.getEntity(), Snowball):
            if isinstance(obj.getEntity().getShooter(), Player):
                plyr = obj.getEntity().getShooter()
                po = g.getPlayer(plyr.getName())
                if po and po.mode == 2:
                    if isinstance(po.mobClass, Enderman):
                        if po.mobClass.isCooldown():  #Dont fire on cooldown derp!
                            c("%sThat action is on cooldown!" % CC.GOLD, obj.getEntity().getShooter().sendMessage)
                            plyr.getInventory().addItem(ItemStack(Material.SNOW_BALL, 1))
                            return obj.setCancelled(True)
                    #snowball.append(obj.getEntity().getUniqueId())
                    return
            obj.setCancelled(True)  #Watch this

# Fire actionThrow when a snowball hits
@hook.event('entity.ProjectileHitEvent')
def projectileHitListener(obj):
    if isinstance(obj.getEntity(), Snowball):
        #if obj.getEntity().getUniqueId() in snowball:
        g.getPlayer(obj.getEntity().getShooter().getName()).mobClass.actionThrow(obj.getEntity().getShooter(), obj.getEntity())
        #snowball.pop(snowball.index(obj.getEntity().getUniqueId()))

    if isinstance(obj.getEntity(), Arrow): #Arrow knockback for ze lulz!
        e = obj.getEntity()
        if isinstance(e, Player):
            po = g.getPlayer(e.getName())
            if po and po.mode == 1 and po.playerClass.name == 'baker':
                for ent in e.getNearbyEntities(4, 4, 4):
                    v = ent.getLocation().toVector().subtract(e.getLocation().toVector()).normalize()
                    ent.setVelocity(v.multiply(1).setY(.6))
                e.getWorld().playSound(e.getLocation(), Sound.EXPLODE, 8, 1)

# Dont allow mobs to remove there armor
@hook.event('inventory.InventoryClickEvent')
def inventoryClickListener(obj):
    p = g.getPlayer(obj.getWhoClicked().getName())
    if obj.getSlotType() == SlotType.ARMOR:  #HELMET
        if p.mode == 2 or obj.getSlot() == 39:
            obj.setCancelled(True)

# Mobs cant pickup items
@hook.event('player.PlayerPickupItemEvent')
def playerPickupItemListener(obj):
    p = obj.getPlayer()
    po = g.getPlayer(p.getName())
    if po and po.mode == 2:
        obj.setCancelled(True)

# Hook kitten-bombs
@hook.event('block.BlockPlaceEvent')
def blockPlaceListener(obj):
    if data['mode'] != 'EMPTY':
        if obj.getBlockPlaced().getLocation().distance(data['blockhash']['egg']) < 5:
            obj.setCancelled(True)
            return obj.getPlayer().sendMessage('%sYou cant place blocks that close too the core!' % CC.YELLOW)
    if obj.getItemInHand().getTypeId() == 390: # Pot
        p = obj.getPlayer()
        po = g.getPlayer(p.getName())
        if po and po.mobClass.name == 'kitty':
            b = obj.getBlockPlaced()
            if b.getLocation().distance(data['blockhash']['egg']) < 25:
                obj.setCancelled(True)
                return p.sendMessage('%sYour bomb must be placed at least 25 blocks away from the core!' % CC.YELLOW)
            if random.randint(1, 5) != 2:
                p.setHealth(0)
                obj.setCancelled(True)
                return p.sendMessage('%sYou should be more careful when you handle explosives...' % CC.RED)
            for i in g.players.values():
                if i.mode == 1 and i.playerClass.name == 'mage':
                    p = server.getPlayer(i.playerName)
                    p.sendMessage('%sSomething doesnt feel right...' % CC.RED)
                    p.setCompassTarget(b.getLocation().add(random.randint(-5, 5), 0, random.randint(-5, 5)))
                    p.saveData()
            loc = b.getLocation()
            bombs[(loc.getX(), loc.getY(), loc.getZ())] = p.getName()
            t = random.choice([60, 70, 80, 90])
            p.sendMessage('%sYour bomb has been placed! T-Minus %s%s%s seconds!' % (CC.YELLOW, CC.RED, t, CC.YELLOW))
            thread.start_new_thread(po.mobClass.bombLoop, (p, loc, t))

# Manage chat stuffs
@hook.event('player.PlayerChatEvent')
def playerChatListener(obj):
    p = obj.getPlayer()
    po = g.getPlayer(p.getName())
    if p.isOp(): ocolor = CC.GOLD
    else: ocolor = CC.GRAY
    if not po:
        server.broadcastMessage('<%s%s%s>: %s' % (ocolor, p.getDisplayName(), CC.RESET, obj.getMessage()))
    elif po:
        if po.mode == 1:
            color, name = po.playerClass.nameplate, po.playerClass.name.title()
        else:
            color, name = CC.RED, po.mobClass.name.title()
        server.broadcastMessage('<[%s%s%s] %s%s%s>: %s' % (color, name, CC.RESET, ocolor, p.getDisplayName(), CC.RESET, obj.getMessage()))
    obj.setCancelled(True)

@hook.event('net.citizensnpcs.api.event.NPCDamageByEntityEvent')
def testNPC(obj):
    if not isinstance(obj.getDamager(), Player): return
    po = g.getPlayer(obj.getDamager().getName())
    if not po or data['mode'] == 'EMPTY':
        obj.getNPC().despawn()
        return obj.getNPC().destroy()
    if po and po.mode != 2 and not po.playerClass.name == 'mage': return
    npc = obj.getNPC().getBukkitEntity()
    obj.getNPC().despawn()
    obj.getNPC().destroy()
    for i in range(1, 5):
        for i in range(1, 9):  #see: http://bit.ly/NtREIP / http://mc.kev009.com/Protocol
            npc.getWorld().playEffect(npc.getLocation(), Effect.SMOKE, i, 3)
    if po.mode == 1: return obj.getDamager().sendMessage('%sBut... But... HE WAS A REAL BOY! D:' % CC.YELLOW)
    obj.getDamager().sendMessage('%sIt was a trick!' % CC.YELLOW)
