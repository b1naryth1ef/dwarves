#Dwarves vs Monsters Bukkit Mod
##By: B1naryth1ef

###Overview
The DvM is an awesome gameplay mod for Bukkit, built around classic defense/horde games. It's a modified version of the original idea, but in this case it's an entire concentrated mod (with only two plugin dependencies)

###Technical Specifications
The plugin is built in Jython, allowing for cleaner and faster development and a slightly smaller memory footprint. To run Jython plugins with Bukkit, you need the [Python Plugin Loader](http://forums.bukkit.org/threads/python-plugin-loader.21247/). To allow for some other nifty features, you will also need [MobDisguise](http://dev.bukkit.org/server-mods/mobdisguise/).

###Using
The mod is still in early development, but the base functionality is there. After installing, you'll need to run a few commands on the server to get setup.    
- `/dwarf new`: sets up a new game (right now, "games" do nothing)    
- `/dwarf cm`: point at a block to set this as the 'monument' where dwarfs spawn and monsters attack    
- `/dwarf origin`: set this in an obsidian box, this is the main server spawnpoint and waiting point for mobs    
- `/dwarf ms`: this is the mob spawn location    
- `/dwarf start`: run this when everyone is on the server and you're ready to start    
- `/dwarf del`: stop and delete the current game    