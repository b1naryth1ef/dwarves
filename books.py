#Py-clone of http://pastie.org/4406416
from net.minecraft.server import NBTBase, NBTTagCompound, NBTTagList, NBTTagString
from org.bukkit import Material
from org.bukkit.craftbukkit.inventory import CraftItemStack
from org.bukkit.inventory import ItemStack
from hashlib import md5

class Book():
    def __init__(self, title="", author="", pages=[]):
        self.title = title
        self.author = author
        self.pages = pages
        self.bd = None
        self.hash = None

    def __eq__(self, obj):
        if isinstance(obj, Book):
            if self.getHash() == obj.getHash():
                return True
        return False

    def cleanup(self):
        for index, page in enumerate(self.pages):
            if len(page) > 200:
                if index+1 == len(self.pages): self.pages.append("")
                self.pages[index] = ''.join(page[:220])
                self.pages[index+1] += ''.join(page[220:])

    def getHash(self):
        if not self.hash:
            self.hash = md5()
            self.hash.update(self.title+self.author+''.join(self.pages))
            self.hash = self.hash.digest()
        return self.hash

    def load(self, item):
        bookData = item.getHandle().tag
        self.author = bookData.getString("author")
        self.title = bookData.getString("title")
        np = bookData.getList("pages")
        self.pages = [np.get(i).toString() for i in range(0, np.size())]
        return self

    def dump(self):
        self.cleanup()
        book = CraftItemStack(Material.WRITTEN_BOOK)
        self.bd = NBTTagCompound()

        self.bd.setString("author", str(self.author))
        self.bd.setString("title", str(self.title))
        pages = NBTTagList()
        for i in self.pages:
            pages.add(NBTTagString(i, i))
        self.bd.set("pages", pages)
        book.getHandle().tag = self.bd
        return book